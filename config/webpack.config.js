const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const autoprefixer = require('autoprefixer');
const eslintFormatter = require('eslint-friendly-formatter');

const Extract = require('extract-text-webpack-plugin');
const HTML = require('html-webpack-plugin');

const pkg = require('../package.json');

const BASE_BUILD_PATH = 'www';
const BUILD_FOLDER = 'build';

const PATHS = {
    app: path.join(__dirname, '..', 'src/ui/'),
    build: path.join(__dirname, '..', BASE_BUILD_PATH, BUILD_FOLDER),
    public: path.posix.join('/', BUILD_FOLDER, '/'),
};

const FILENAMES = {
    build: 'js/[name].bundle.js',
    vendor: 'js/[name].bundle.js',
    css: 'css/[name].css',
    scss: 'css/[name].scss.css',
    font: 'fonts/[hash].[ext]',
    image: 'images/[hash].[ext]',
    template: 'src/templates/index.html',
    html: '../index.html',
};

const extractCSS = new Extract({
    filename: FILENAMES.css,
});
const extractSCSS = new Extract({
    filename: FILENAMES.scss,
});

const commonConfig = {
    entry: {
        app: PATHS.app,
    },
    output: {
        path: PATHS.build,
        filename: FILENAMES.build,
        publicPath: PATHS.public,
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['es2015', 'react', 'stage-1'],
                            compact: false,
                        },
                    },
                    {
                        loader: 'eslint-loader',
                        options: {
                            formatter: eslintFormatter,
                        },
                    },
                ],
            },
            {
                test: /\.scss$/,
                loader: extractSCSS.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: [autoprefixer({ browsers: ['last 2 versions'] })],
                            },
                        },
                        {
                            loader: 'sass-loader?indentedSyntax',
                        },
                    ],
                }),
            },
            {
                test: /\.css$/,
                loader: extractCSS.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                        },
                    ],
                }),
            },
            {
                test: /\.(ttf|otf|eot|woff[2]?|svg)(\?(v=)?[\d.]*)?$/,
                loader: `file-loader?name=${FILENAMES.font}`,
            },
            {
                test: /\.(jpe?g|png|gif|ico)$/i,
                loader: `url-loader?name=${FILENAMES.image}&limit=25000/`,
            },
            {
                test: /\.yml$/,
                loader: 'json-loader!yaml-loader',
            },
            {
                test: /\.json$/,
                loader: 'json-loader',
            },
        ],
    },
    node: {
        net: 'empty',
        tls: 'empty',
        dns: 'empty',
    },
};

const devConfig = {
    devServer: {
        historyApiFallback: true,
        contentBase: BASE_BUILD_PATH,
    },
    devtool: 'eval',
};

const stageConfig = {
    devtool: 'source-map',
};

const prodConfig = {
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
            },
        }),
    ],
    devtool: 'source-map',
};

function commonPlugins(options) {
    return {
        entry: {
            vendor: options.vendors,
        },
        plugins: [
            new webpack.optimize.CommonsChunkPlugin({
                name: 'vendor',
                filename: FILENAMES.vendor,
            }),
            new webpack.optimize.OccurrenceOrderPlugin(),
            new webpack.NoEmitOnErrorsPlugin(),
            new webpack.EnvironmentPlugin([
                'NODE_ENV',
            ]),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
            }),
            new webpack.DefinePlugin({
                __DEV__: JSON.stringify(JSON.parse(process.env.DEBUG || 'false')),
            }),
            extractCSS,
            extractSCSS,
            new HTML({
                filename: FILENAMES.html,
                template: options.template,
                inject: 'body',
            }),
        ],
    };
}

// eslint-disable-next-line no-console
console.warn(`Building with NODE_ENV = ${process.env.NODE_ENV}`);

let config = merge(
    commonConfig,
    commonPlugins({
        vendors: Object.keys(pkg.dependencies),
        template: FILENAMES.template,
    })
);

switch (process.env.NODE_ENV) {
case 'development':
    config = merge(config, devConfig);
    break;
case 'stage':
    config = merge(config, stageConfig);
    break;
case 'production':
    config = merge(config, prodConfig);
    break;
default:
    // eslint-disable-next-line no-console
    console.warn(`Unspecified config for NODE ENV = ${process.env.NODE_ENV}`);
}

module.exports = config;
