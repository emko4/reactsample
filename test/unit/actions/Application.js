import configureMockStore from 'redux-mock-store';

import { expect } from '../../testHelper';

import * as ApplicationActions from '../../../src/ui/actions/ApplicationActions';

import * as ApplicationTypes from '../../../src/ui/constants/ApplicationTypes';

const mockStore = configureMockStore();

describe('Application', () => {
    describe('Menu actions', () => {
        it('Menu open action', () => {
            const store = mockStore();

            store.dispatch(ApplicationActions.menuOpen());

            const expected = [{
                type: ApplicationTypes.MENU_OPEN,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Menu close action', () => {
            const store = mockStore();

            store.dispatch(ApplicationActions.menuClose());

            const expected = [{
                type: ApplicationTypes.MENU_CLOSE,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Menu toggle action', () => {
            const store = mockStore();

            store.dispatch(ApplicationActions.menuToggle());

            const expected = [{
                type: ApplicationTypes.MENU_TOGGLE,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });
    });
});
