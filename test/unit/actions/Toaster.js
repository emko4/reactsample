import configureMockStore from 'redux-mock-store';

import { expect } from '../../testHelper';

import * as ToastActions from '../../../src/ui/actions/ToastActions';

import * as ToastTypes from '../../../src/ui/constants/ToastTypes';

const mockStore = configureMockStore();

describe('Toaster', () => {
    describe('Toaster actions', () => {
        it('Add general toast action', () => {
            const store = mockStore();

            store.dispatch(ToastActions.addToast({
                kind: 'success',
                title: 'Title',
                message: 'Message',
            }));

            const expected = [{
                type: ToastTypes.ADD_TOAST,
                kind: 'success',
                title: 'Title',
                message: 'Message',
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Add success toast action', () => {
            const store = mockStore();

            store.dispatch(ToastActions.addSuccessToast({
                title: 'Title',
                message: 'Message',
            }));

            const expected = [{
                type: ToastTypes.ADD_TOAST,
                kind: 'success',
                title: 'Title',
                message: 'Message',
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Add info toast action', () => {
            const store = mockStore();

            store.dispatch(ToastActions.addInfoToast({
                title: 'Title',
                message: 'Message',
            }));

            const expected = [{
                type: ToastTypes.ADD_TOAST,
                kind: 'info',
                title: 'Title',
                message: 'Message',
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Add warning toast action', () => {
            const store = mockStore();

            store.dispatch(ToastActions.addWarningToast({
                title: 'Title',
                message: 'Message',
            }));

            const expected = [{
                type: ToastTypes.ADD_TOAST,
                kind: 'warning',
                title: 'Title',
                message: 'Message',
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Add error toast action', () => {
            const store = mockStore();

            store.dispatch(ToastActions.addErrorToast({
                title: 'Title',
                message: 'Message',
            }));

            const expected = [{
                type: ToastTypes.ADD_TOAST,
                kind: 'error',
                title: 'Title',
                message: 'Message',
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Add API error toast action', () => {
            const store = mockStore();
            const error = {
                code: 404,
                caption: 'Api Error',
            };

            store.dispatch(ToastActions.addApiErrorToast(error));

            const expected = [{
                type: ToastTypes.ADD_TOAST,
                kind: 'error',
                message: 'toast.apiError.message',
                title: 'toast.apiError.title',
                params: {
                    ...error,
                },
                error,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Add application error toast action', () => {
            const store = mockStore();
            const error = {
                code: 404,
                caption: 'Api Error',
            };

            store.dispatch(ToastActions.addApplicationErrorToast(error));

            const expected = [{
                type: ToastTypes.ADD_TOAST,
                kind: 'error',
                message: 'toast.applicationError.message',
                title: 'toast.applicationError.title',
                error,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Clear actual toasts action', () => {
            const store = mockStore();

            store.dispatch(ToastActions.clearActualToast());

            const expected = [{
                type: ToastTypes.CLEAR_ACTUAL_TOAST,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });
    });
});
