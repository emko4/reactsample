import configureMockStore from 'redux-mock-store';

import { expect } from '../../testHelper';

import * as DialogActions from '../../../src/ui/actions/DialogActions';

import * as DialogTypes from '../../../src/ui/constants/DialogTypes';

const mockStore = configureMockStore();

describe('Dialog', () => {
    describe('Yes/No dialog actions', () => {
        it('Show dialog action', () => {
            const store = mockStore();
            const onYes = () => {};
            const onNo = () => {};

            store.dispatch(DialogActions.showYesNoDialog({
                title: '',
                message: '',
                yesLabel: 'Yes',
                noLabel: 'No',
            }, onYes, onNo));

            const expected = [{
                type: DialogTypes.SHOW_YES_NO_DIALOG,
                yesNo: {
                    open: true,
                    title: '',
                    message: '',
                    yesLabel: 'Yes',
                    noLabel: 'No',
                },
                onYes,
                onNo,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Hide dialog action', () => {
            const store = mockStore();
            const callback = () => {};

            store.dispatch(DialogActions.hideYesNoDialog(callback));

            const expected = [{
                type: DialogTypes.HIDE_YES_NO_DIALOG,
                callback,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });
    });
});
