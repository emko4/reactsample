import configureMockStore from 'redux-mock-store';
import multi from 'redux-multi';

import { expect } from '../../testHelper';

import * as AuthActions from '../../../src/ui/actions/AuthActions';
import * as AuthTypes from '../../../src/ui/constants/AuthTypes';
import * as UserActions from '../../../src/ui/actions/UserActions';
import * as UserTypes from '../../../src/ui/constants/UserTypes';

import * as ApplicationTypes from '../../../src/ui/constants/ApplicationTypes';
import * as ToastTypes from '../../../src/ui/constants/ToastTypes';

const mockStore = configureMockStore([multi]);

describe('Authorization', () => {
    describe('Sign In actions', () => {
        it('Sign In Request action', () => {
            const store = mockStore();
            const onComplete = () => {};

            store.dispatch(AuthActions.signInRequest({
                email: 'test@test.cz',
                password: 'abc',
            }, onComplete));

            const expected = [{
                type: AuthTypes.REQUEST_SIGN_IN,
                onError: AuthActions.signInFailed,
                onSuccess: AuthActions.signIn,
                onComplete,
                response: {
                    user: {
                        id: 1,
                        username: 'test@test.cz',
                        email: 'test@test.cz',
                        roles: [
                            {
                                id: 1,
                                name: 'ADMIN',
                            },
                        ],
                    },
                },
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Sign In action', () => {
            const store = mockStore();

            store.dispatch(AuthActions.signIn({
                user: {
                    id: 1,
                    username: 'test@test.cz',
                    email: 'test@test.cz',
                    roles: [
                        {
                            id: 1,
                            name: 'ADMIN',
                        },
                    ],
                },
            }));

            const expected = [{
                type: AuthTypes.SIGN_IN,
                user: {
                    id: 1,
                    username: 'test@test.cz',
                    email: 'test@test.cz',
                    roles: [
                        {
                            id: 1,
                            name: 'ADMIN',
                        },
                    ],
                },
            }, {
                type: UserTypes.REQUEST_GET_ME,
                onSuccess: UserActions.getMe,
                onError: UserActions.getMeFailed,
                onComplete: null,
                response: {
                    id: 1,
                    username: 'test@test.cz',
                    email: 'test@test.cz',
                    roles: [
                        {
                            id: 1,
                            name: 'ADMIN',
                        },
                    ],
                },
            }, {
                type: ToastTypes.ADD_TOAST,
                kind: 'success',
                message: 'toast.signIn.logged',
                params: {
                    username: 'test@test.cz',
                },
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Sign In Failed action', () => {
            const store = mockStore();
            const error = {
                code: 400,
            };

            store.dispatch(AuthActions.signInFailed(error));

            const expected = [{
                type: AuthTypes.SIGN_IN_FAILED,
            }, {
                type: ToastTypes.ADD_TOAST,
                kind: 'error',
                title: 'toast.signIn.title',
                error,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });
    });

    describe('Sign Out actions', () => {
        it('Sign Out action', () => {
            const store = mockStore();

            store.dispatch(AuthActions.signOut());

            const expected = [{
                type: AuthTypes.SIGN_OUT,
                redirect: true,
            }, {
                type: UserTypes.CLEAR_USER,
            }, {
                type: ApplicationTypes.MENU_CLOSE,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });
    });
});
