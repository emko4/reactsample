import configureMockStore from 'redux-mock-store';
import multi from 'redux-multi';

import { expect } from '../../testHelper';

import * as UserActions from '../../../src/ui/actions/UserActions';

import * as UserTypes from '../../../src/ui/constants/UserTypes';
import * as ToastTypes from '../../../src/ui/constants/ToastTypes';

const mockStore = configureMockStore([multi]);

const userObject = {
    id: 1,
    username: 'test@test.cz',
    email: 'test@test.cz',
    roles: [
        {
            id: 1,
            name: 'ADMIN',
        },
    ],
};

describe('User', () => {
    describe('Get Me actions', () => {
        it('Get Me Request action', () => {
            const store = mockStore();
            const onComplete = () => {};

            store.dispatch(UserActions.getMeRequest(userObject, onComplete));

            const expected = [{
                type: UserTypes.REQUEST_GET_ME,
                onError: UserActions.getMeFailed,
                onSuccess: UserActions.getMe,
                onComplete,
                response: userObject,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Get Me action', () => {
            const store = mockStore();

            store.dispatch(UserActions.getMe(userObject));

            const expected = [{
                type: UserTypes.GET_ME,
                user: userObject,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });

        it('Get Me Failed action', () => {
            const store = mockStore();
            const error = {
                code: 403,
            };

            store.dispatch(UserActions.getMeFailed(error));

            const expected = [{
                type: UserTypes.GET_ME_FAILED,
            }, {
                type: ToastTypes.ADD_TOAST,
                kind: 'error',
                title: 'toast.getMe.title',
                error,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });
    });

    describe('Clear actions', () => {
        it('Clear User action', () => {
            const store = mockStore();

            store.dispatch(UserActions.clearUser());

            const expected = [{
                type: UserTypes.CLEAR_USER,
            }];

            expect(store.getActions()).to.deep.equal(expected);
        });
    });
});
