import jq from 'jquery';
import React from 'react';
import ReactTestUtils from 'react-addons-test-utils';
import jsdom from 'jsdom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { expect } from 'chai';
import reducers from '../src/ui/reducers';

global.document = jsdom.jsdom('<!doctype html><html><body></body></html>');
global.window = global.document.defaultView;
const $ = jq(window);

function renderComponent(ComponentClass, props = {}, state = {}) {
    let providerRef = null;
    ReactTestUtils.renderIntoDocument(
        <Provider ref={(r) => { providerRef = r; }} store={createStore(reducers, state)}>
            <ComponentClass {...props} />
        </Provider>
    );

    // Produces HTML
    return $(providerRef);
}

export { renderComponent, expect };
