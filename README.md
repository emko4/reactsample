# React-redux Sample

## Development (localhost)

* Install packages by [Yarn](https://yarnpkg.com/en/)
    * `yarn`
* Run npm script develop by [Yarn](https://yarnpkg.com/en/)
    * `yarn develop` 
* Run npm script start by [Yarn](https://yarnpkg.com/en/) 
    * `yarn start`
* Dev server is running on http://localhost:8080    

## Production

* Install packages by [Yarn](https://yarnpkg.com/en/)
    * `yarn`
* Run npm script deploy by [Yarn](https://yarnpkg.com/en/) 
    * `yarn deploy`
* Production files are in *www* folder

# React-redux Sample

Využívá balíčkovací systém **yarn** pro instalování js knihoven, **Webpack** pro skládání modulů do jednoho bundle, **Babel** pro kompilaci ES6 a JSX do ES5 a JS polyfilly, **React** pro tvorbu uživatelských rozhraní a **Redux** pro udržování stavu aplikace.

## Začínáme

Pro nainstalování balíčků a pouštění scriptů se používá [Yarn](https://yarnpkg.com/en/)

### Development

* Pro build aplikace v externě nastaveném prostředí (NODE_ENV)
    * `$ yarn run build`
    * otevřete soubor `index.html` na webserveru

* Pro build aplikace v **development** prostředí:
    * `$ yarn run develop`
    * otevřete soubor `index.html` na webserveru
    
* Pro spuštění **webpack-dev-serveru** s livereload: 
    * `$ yarn run start`
    * PŘED PRVNÍM SPUŠTĚNÍM SERVERU JE POTŘEBA PUSTIT SCRIPT DEVELOP, ABY SE VYTVOŘIL INDEX.HTML, KTERÝ SERVER NEVYTVÁŘÍ
    * otevřete prohlížeč na http://localhost/8080

### Testing

Pro spuštění testů použijte:

```sh
yarn run test
```
