import React, { Component, PropTypes } from 'react';

import CircularProgress from 'material-ui/CircularProgress';

class Loader extends Component {

    static propTypes = {
        backgroundStyle: PropTypes.object,
        bodyStyle: PropTypes.object,
        children: PropTypes.object,
        show: PropTypes.bool.isRequired,
        style: PropTypes.object,
    };

    render() {
        const loaderStyles = this.props.style || {};
        const backgroundStyle = this.props.backgroundStyle || {};
        const bodyStyles = this.props.bodyStyle || {};

        if (this.props.show) {
            return (
                <div className="loader" style={loaderStyles}>
                    <div className="loader__background" style={backgroundStyle} />
                    <div className="loader__body" style={bodyStyles}>
                        <CircularProgress size={60} thickness={5} />
                    </div>
                    {this.props.children}
                </div>
            );
        }

        return (
            <div>{this.props.children}</div>
        );
    }

}

export default Loader;
