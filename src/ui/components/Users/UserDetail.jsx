import React, { Component, PropTypes } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

const styles = {
    wrap: {
        cursor: 'auto',
    },
    input: {
        cursor: 'auto',
        color: 'rgba(0, 0, 0, 0.498039)',
        fontWeight: 'bold',
    },
    underlineDisable: {
        cursor: 'auto',
        borderBottomStyle: 'solid',
        borderBottomWidth: '1px',
    },
};

const texts = {
    title: <FormattedMessage id="user.detail.title" />,
    email: <FormattedMessage id="user.detail.email.title" />,
    name: <FormattedMessage id="user.detail.name.title" />,
    surname: <FormattedMessage id="user.detail.surname.title" />,
};

class UserDetail extends Component {

    static propTypes = {
        intl: PropTypes.object.isRequired,
        user: PropTypes.object.isRequired,
    };

    render() {
        const user = this.props.user || {};

        const localTexts = {
            loading: this.props.intl.formatMessage({ id: 'loading' }),
        };

        return (
            <Paper className="paper">
                <h3><FormattedMessage id="user.detail.title" /></h3>
                <div className="spacer" />
                <TextField
                    type="text"
                    style={styles.wrap}
                    inputStyle={styles.input}
                    underlineDisabledStyle={styles.underlineDisable}
                    fullWidth
                    value={user.email || localTexts.loading}
                    disabled
                    floatingLabelText={texts.email}
                    floatingLabelFixed
                />
                <div className="spacer" />
            </Paper>
        );
    }

}

export default injectIntl(UserDetail);
