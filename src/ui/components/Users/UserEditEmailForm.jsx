import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';
import { Form, Field, reduxForm } from 'redux-form';

import _ from 'lodash';
import Joi from 'joi-browser';

import TextField from '../FormFields/TextField';
import RaisedButton from '../FormFields/RaisedButton';

const texts = {
    email: {
        hint: (<FormattedMessage id={'user.changeEmail.email.hint'} />),
        label: (<FormattedMessage id={'user.changeEmail.email.label'} />),
    },
    emailCheck: {
        hint: (<FormattedMessage id={'user.changeEmail.emailCheck.hint'} />),
        label: (<FormattedMessage id={'user.changeEmail.emailCheck.label'} />),
    },
    save: (<FormattedMessage id={'user.changeEmail.save'} />),
    reset: (<FormattedMessage id={'user.changeEmail.reset'} />),
};

const validate = (values = {}) => {
    const errors = {};

    // not same
    if (!_.isEqual(values.email, values.emailCheck)) {
        errors.email = <FormattedMessage id={'user.changeEmail.error.same'} />;
        errors.emailCheck = <FormattedMessage id={'user.changeEmail.error.same'} />;
    }

    // incorrect
    if (Joi.validate(values.email, Joi.string().email()).error) {
        errors.email = <FormattedMessage id={'user.changeEmail.email.error.incorrect'} />;
    }
    if (Joi.validate(values.emailCheck, Joi.string().email()).error) {
        errors.emailCheck = <FormattedMessage id={'user.changeEmail.email.error.incorrect'} />;
    }

    // required
    if (!values.email) {
        errors.email = <FormattedMessage id={'user.changeEmail.email.error.isRequired'} />;
    }
    if (!values.emailCheck) {
        errors.emailCheck = <FormattedMessage id={'user.changeEmail.emailCheck.error.isRequired'} />;
    }

    return errors;
};

class UserEditEmailForm extends Component {

    static propTypes = {
        handleSubmit: PropTypes.func.isRequired,
        invalid: PropTypes.bool.isRequired,
        onSubmit: PropTypes.func.isRequired,
        pristine: PropTypes.bool.isRequired,
        reset: PropTypes.func.isRequired,
    };

    render() {
        const { handleSubmit, reset, pristine, invalid, onSubmit } = this.props;

        return (
            <Form className="form" onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <Field name={'email'} component={TextField} hint={texts.email.hint} label={texts.email.label} />
                </div>
                <div>
                    <Field name={'emailCheck'} component={TextField} hint={texts.emailCheck.hint} label={texts.emailCheck.label} />
                </div>
                <div className="spacer spacer--double" />
                <div className="text-center">
                    <div className="form__button">
                        <Field
                            name="submitButton"
                            component={RaisedButton}
                            type={'submit'}
                            label={texts.save}
                            disabled={pristine || invalid}
                            primary />
                    </div>
                    <div className="form__button">
                        <Field
                            name="resetButton"
                            component={RaisedButton}
                            label={texts.reset}
                            disabled={pristine}
                            secondary
                            onTouchTap={reset} />
                    </div>
                </div>
            </Form>
        );
    }
}

export default reduxForm({
    form: 'UserEditEmailForm',
    validate,
    enableReinitialize: true,
})(UserEditEmailForm);
