import React, { Component, PropTypes } from 'react';

import UserEditPasswordForm from './UserEditPasswordForm';

class UserEditPassword extends Component {

    static propTypes = {
        savePassword: PropTypes.func.isRequired,
    };

    render() {
        return (
            <div className="paper">
                <UserEditPasswordForm onSubmit={this.props.savePassword} />
            </div>
        );
    }

}

export default UserEditPassword;
