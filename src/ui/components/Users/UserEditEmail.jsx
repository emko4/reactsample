import React, { Component, PropTypes } from 'react';

import UserEditEmailForm from './UserEditEmailForm';

class UserEditEmail extends Component {

    static propTypes = {
        saveEmail: PropTypes.func.isRequired,
    };

    render() {
        return (
            <div className="paper">
                <UserEditEmailForm onSubmit={this.props.saveEmail} />
            </div>
        );
    }

}

export default UserEditEmail;
