import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';

import _ from 'lodash';

import Avatar from 'material-ui/Avatar';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

const texts = {
    loading: <FormattedMessage id={'loading'} />,
    signOut: <FormattedMessage id={'menu.signOut'} />,
};

const icons = {
    signOut: <FontIcon className="fa fa-sign-out" />,
};

class LoggedUser extends Component {

    static propTypes = {
        onSignOut: PropTypes.func.isRequired,
        textColor: PropTypes.string,
        username: PropTypes.string.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            open: false,
            anchorOrigin: {
                horizontal: 'right',
                vertical: 'bottom',
            },
            targetOrigin: {
                horizontal: 'right',
                vertical: 'top',
            },
        };
    }

    onUserClick = (event) => {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            open: true,
            anchorEl: event.currentTarget,
        });
    };

    onCloseMenu = () => {
        this.setState({
            open: false,
        });
    };

    signOut = () => {
        this.props.onSignOut();
    }

    render() {
        const localStyles = {
            label: {
                color: this.props.textColor || '#ffffff',
            },
        };

        let avatar = null;
        let username = texts.loading;

        if (!_.isEmpty(this.props.username)) {
            avatar = <Avatar size={30}>{_.toUpper(_.first(this.props.username))}</Avatar>;
            username = this.props.username;
        }

        return (
            <div className="logged-user">
                <FlatButton label={username} labelPosition="before" icon={avatar} onTouchTap={this.onUserClick} labelStyle={localStyles.label} />
                <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchorEl}
                    onRequestClose={this.onCloseMenu}
                    anchorOrigin={this.state.anchorOrigin}
                    targetOrigin={this.state.targetOrigin}
                >
                    <Menu>
                        <MenuItem primaryText={texts.signOut} onTouchTap={this.signOut} leftIcon={icons.signOut} />
                    </Menu>
                </Popover>
            </div>
        );
    }

}

export default LoggedUser;
