import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';
import { Form, Field, reduxForm } from 'redux-form';

import _ from 'lodash';

import TextField from '../FormFields/TextField';
import RaisedButton from '../FormFields/RaisedButton';

const texts = {
    oldPass: {
        hint: (<FormattedMessage id={'user.changePassword.old.hint'} />),
        label: (<FormattedMessage id={'user.changePassword.old.label'} />),
    },
    newPass: {
        hint: (<FormattedMessage id={'user.changePassword.new.hint'} />),
        label: (<FormattedMessage id={'user.changePassword.new.label'} />),
    },
    newPassCheck: {
        hint: (<FormattedMessage id={'user.changePassword.newAgain.hint'} />),
        label: (<FormattedMessage id={'user.changePassword.newAgain.label'} />),
    },
    save: (<FormattedMessage id={'user.changePassword.save'} />),
    reset: (<FormattedMessage id={'user.changePassword.reset'} />),
};

const validate = (values = {}) => {
    const errors = {};

    // not same
    if (!_.isEqual(values.passwordNew, values.passwordNewCheck)) {
        errors.passwordNew = <FormattedMessage id={'user.changePassword.error.same'} />;
        errors.passwordNewCheck = <FormattedMessage id={'user.changePassword.error.same'} />;
    }

    // required
    if (!values.passwordOld) {
        errors.passwordOld = <FormattedMessage id={'user.changePassword.old.error.isRequired'} />;
    }
    if (!values.passwordNew) {
        errors.passwordNew = <FormattedMessage id={'user.changePassword.new.error.isRequired'} />;
    }
    if (!values.passwordNewCheck) {
        errors.passwordNewCheck = <FormattedMessage id={'user.changePassword.newAgain.error.isRequired'} />;
    }

    return errors;
};

class UserEditPasswordForm extends Component {

    static propTypes = {
        handleSubmit: PropTypes.func.isRequired,
        invalid: PropTypes.bool.isRequired,
        onSubmit: PropTypes.func.isRequired,
        pristine: PropTypes.bool.isRequired,
        reset: PropTypes.func.isRequired,
    };

    render() {
        const { handleSubmit, reset, pristine, invalid, onSubmit } = this.props;

        return (
            <Form className="form" onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <Field
                        name={'passwordOld'}
                        component={TextField}
                        type={'password'}
                        hint={texts.oldPass.hint}
                        label={texts.oldPass.label} />
                </div>
                <div>
                    <Field
                        name={'passwordNew'}
                        component={TextField}
                        type={'password'}
                        hint={texts.newPass.hint}
                        label={texts.newPass.label} />
                </div>
                <div>
                    <Field
                        name={'passwordNewCheck'}
                        component={TextField}
                        type={'password'}
                        hint={texts.newPassCheck.hint}
                        label={texts.newPassCheck.label} />
                </div>
                <div className="spacer spacer--double" />
                <div className="text-center">
                    <div className="form__button">
                        <Field
                            name="submitButton"
                            component={RaisedButton}
                            type={'submit'}
                            label={texts.save}
                            disabled={pristine || invalid}
                            primary />
                    </div>
                    <div className="form__button">
                        <Field
                            name="resetButton"
                            component={RaisedButton}
                            label={texts.reset}
                            disabled={pristine}
                            secondary
                            onTouchTap={reset} />
                    </div>
                </div>
            </Form>
        );
    }
}

export default reduxForm({
    form: 'UserEditPasswordForm',
    validate,
    enableReinitialize: true,
})(UserEditPasswordForm);
