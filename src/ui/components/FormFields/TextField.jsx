import React, { Component, PropTypes } from 'react';

import MuiTextField from 'material-ui/TextField';

export default class TextField extends Component {

    static propTypes = {
        hint: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
        ]),
        input: PropTypes.object.isRequired,
        label: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
        ]),
        meta: PropTypes.object,
        prefix: PropTypes.string,
        suffix: PropTypes.string,
        type: PropTypes.string,
    };

    static defaultProps = {
        prefix: '',
        suffix: '',
    };

    componentWillMount() {
        this.setInitialState(this.props);
    }

    componentWillReceiveProps(nextProps) {
        const actVisited = this.props.meta.visited;
        const actPristine = this.props.meta.pristine;
        const nxtPristine = nextProps.meta.pristine;
        const actValue = this.props.input.value;
        const nxtValue = nextProps.input.value;

        // initial value
        if (!actVisited && (actValue !== nxtValue)) {
            this.setInitialState(nextProps);
        }

        // reset
        if (nxtPristine && (actPristine !== nxtPristine)) {
            this.setInitialState(nextProps);
        }
    }

    onChange = (_e, text) => {
        const { input } = this.props;

        if (input) {
            input.onChange(text);
        }

        this.setState({
            value: text,
            muiText: text,
        });
    };

    onFocus = () => {
        const { input } = this.props;

        if (input) {
            input.onFocus();
        }

        this.setState({
            muiText: this.state.value,
        });
    };

    onBlur = () => {
        const { input, prefix, suffix } = this.props;

        if (input) {
            input.onBlur();
        }

        this.setState({
            muiText: `${prefix}${this.state.value}${suffix}`,
        });
    };

    setInitialState = (actProps) => {
        const { input, prefix, suffix } = actProps;

        const text = input.value ? input.value : '';

        this.setState({
            value: text,
            muiText: `${prefix}${text}${suffix}`,
        });
    };

    render() {
        const { input, hint, label, type = 'text', meta: { touched, error } } = this.props;

        return (
            <div>
                <MuiTextField
                    hintText={hint}
                    floatingLabelText={label}
                    errorText={touched && error}
                    fullWidth
                    floatingLabelFixed
                    autoComplete={'off'}
                    type={type}
                    onChange={this.onChange}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    value={this.state.muiText}
                />
                <input
                    type={'hidden'}
                    {...input}
                    value={this.state.value}
                />
            </div>
        );
    }

}
