import React, { Component, PropTypes } from 'react';

import MuiRaisedButton from 'material-ui/RaisedButton';

export default class RaisedButton extends Component {

    static propTypes = {
        disabled: PropTypes.bool,
        label: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object,
        ]),
        onTouchTap: PropTypes.func,
        primary: PropTypes.bool,
        secondary: PropTypes.bool,
        type: PropTypes.string,
    };

    render() {
        const { label, type = 'button', disabled, onTouchTap = () => {}, primary = false, secondary = false } = this.props;

        return (
            <MuiRaisedButton
                label={label}
                primary={primary}
                secondary={secondary}
                type={type}
                disabled={disabled}
                onTouchTap={onTouchTap}
            />
        );
    }

}
