import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';

import FontIcon from 'material-ui/FontIcon';
import { List, ListItem } from 'material-ui/List';

const texts = {
    languages: <FormattedMessage id="menu.languages" />,
    english: <FormattedMessage id="languages.english" />,
    czech: <FormattedMessage id="languages.czech" />,
};

const icons = {
    language: <FontIcon className="fa fa-language" />,
};

class LanguageChooser extends Component {

    static propTypes = {
        outOfLayout: PropTypes.bool,
        translateActions: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            open: false,
            docked: false,
            mql: null,
        };
    }

    setLocale = (locale) => {
        this.props.translateActions.setLocale(locale);
    };

    render() {
        const languageList = [
            <ListItem key={1} primaryText={texts.english} onTouchTap={this.setLocale.bind(this, 'en')} />,
            <ListItem key={2} primaryText={texts.czech} onTouchTap={this.setLocale.bind(this, 'cs')} />,
        ];

        return (
            <div className={`language-chooser${this.props.outOfLayout ? ' language-chooser--out' : ''}`}>
                <List>
                    <ListItem primaryText={texts.languages} leftIcon={icons.language} nestedItems={languageList} primaryTogglesNestedList />
                </List>
            </div>
        );
    }

}

export default LanguageChooser;
