import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';

import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import { List, ListItem } from 'material-ui/List';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

const texts = {
    menu: <FormattedMessage id="menu.title" />,
    home: <FormattedMessage id="menu.home" />,
    user: <FormattedMessage id="menu.user.user" />,
    userDetail: <FormattedMessage id="menu.user.detail" />,
    userEdit: <FormattedMessage id="menu.user.edit" />,
    userDelete: <FormattedMessage id="menu.user.delete" />,
    languages: <FormattedMessage id="menu.languages" />,
    english: <FormattedMessage id="languages.english" />,
    czech: <FormattedMessage id="languages.czech" />,
    signOut: <FormattedMessage id="menu.signOut" />,
};

const icons = {
    home: <FontIcon className="fa fa-home" />,
    user: <FontIcon className="fa fa-user" />,
    userDetail: <FontIcon className="fa fa-user-circle-o" />,
    userEdit: <FontIcon className="fa fa-pencil" />,
    userDelete: <FontIcon className="fa fa-user-times" />,
    language: <FontIcon className="fa fa-language" />,
    signOut: <FontIcon className="fa fa-sign-out" />,
    cs: <FontIcon className="flag flag-icon flag-icon-cz" />,
    en: <FontIcon className="flag flag-icon flag-icon-gb" />,
};

class Menu extends Component {

    static propTypes = {
        appActions: PropTypes.object.isRequired,
        authActions: PropTypes.object.isRequired,
        dialogActions: PropTypes.object.isRequired,
        intl: PropTypes.object.isRequired,
        locale: PropTypes.string.isRequired,
        menuOpen: PropTypes.bool,
        routerActions: PropTypes.object.isRequired,
        toastActions: PropTypes.object.isRequired,
        translateActions: PropTypes.object.isRequired,
    };

    setLocale = (locale) => {
        // No change, if same languages
        if (locale === this.props.locale) return;

        this.props.dialogActions.showYesNoDialog({
            title: this.props.intl.formatMessage({ id: 'dialogs.setLocale.title' }),
            message: this.props.intl.formatMessage({ id: 'dialogs.setLocale.message' }),
            yesLabel: this.props.intl.formatMessage({ id: 'dialogs.setLocale.yesButton' }),
            noLabel: this.props.intl.formatMessage({ id: 'dialogs.setLocale.noButton' }),
        }, () => {
            this.props.translateActions.setLocale(locale);
            this.toggleMenu();
        });
    };

    signOut = () => {
        this.props.authActions.signOut();
    };

    userDelete = () => {
        this.props.dialogActions.showYesNoDialog({
            title: this.props.intl.formatMessage({ id: 'dialogs.deleteUser.title' }),
            message: this.props.intl.formatMessage({ id: 'dialogs.deleteUser.message' }),
            yesLabel: this.props.intl.formatMessage({ id: 'dialogs.deleteUser.yesButton' }),
            noLabel: this.props.intl.formatMessage({ id: 'dialogs.deleteUser.noButton' }),
        }, () => {
            this.props.toastActions.addInfoToast({
                message: 'menu.toast.deleteUser.warning',
                options: {
                    progressBar: true,
                },
            });
        });
    };

    redirect = (url) => {
        // safety remove double slashes
        const replacedUrl = url.replace(/\/\//igm, '/');
        this.props.routerActions.push(replacedUrl);
        this.toggleMenu();
    };

    toggleMenu = () => {
        this.props.appActions.menuToggle();
    };

    render() {
        const closeIcon = <IconButton><NavigationClose /></IconButton>;

        /* HOME MENU */

        const home = <ListItem primaryText={texts.home} leftIcon={icons.home} onTouchTap={this.redirect.bind(this, '/')} />;

        /* USER MENU */

        const userList = [
            <ListItem
                key={1}
                leftIcon={icons.userDetail}
                primaryText={texts.userDetail}
                onTouchTap={this.redirect.bind(this, '/user')} />,
            <ListItem
                key={2}
                leftIcon={icons.userEdit}
                primaryText={texts.userEdit}
                onTouchTap={this.redirect.bind(this, '/user/edit')} />,
            <ListItem
                key={3}
                leftIcon={icons.userDelete}
                primaryText={texts.userDelete}
                onTouchTap={this.userDelete.bind(this)} />,
        ];

        const user = (
            <ListItem
                primaryText={texts.user}
                leftIcon={icons.user}
                nestedItems={userList}
                primaryTogglesNestedList />
        );

        /* LANGUAGE MENU */

        const languageList = [
            <ListItem
                key={1}
                primaryText={texts.english}
                rightIcon={icons.en}
                onTouchTap={this.setLocale.bind(this, 'en')} />,
            <ListItem
                key={2}
                primaryText={texts.czech}
                rightIcon={icons.cs}
                onTouchTap={this.setLocale.bind(this, 'cs')} />,
        ];

        /* RENDER MENU */

        return (
            <Drawer open={this.props.menuOpen} docked={false} onRequestChange={this.toggleMenu}>
                <AppBar
                    title={texts.menu}
                    iconElementLeft={closeIcon}
                    onLeftIconButtonTouchTap={this.toggleMenu} />
                <List>
                    {home}
                    {user}
                    <ListItem primaryText={texts.languages} leftIcon={icons.language} nestedItems={languageList} primaryTogglesNestedList />
                    <ListItem primaryText={texts.signOut} leftIcon={icons.signOut} onTouchTap={this.signOut} />
                </List>
            </Drawer>
        );
    }

}

export default Menu;
