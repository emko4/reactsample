import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';

import Paper from 'material-ui/Paper';

import config from '../../config/config';

class Homepage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            itemCount: 0,
        };
    }

    componentWillMount() {
        this.testTimer = setInterval(() => {
            const newCount = (this.state.itemCount < 5) ? (this.state.itemCount + 1) : 0;
            this.setState({
                itemCount: newCount,
            });
        }, 2000);
    }

    componentWillUnmount() {
        clearInterval(this.testTimer);
    }

    render() {
        return (
            <Paper className="homepage">
                <h1><FormattedMessage id="homepage.text" values={{ appName: config.appName }} /></h1>
                <div className="spacer" />
                <h4 style={{ color: '#00bcd4' }}>
                    <FormattedMessage id="homepage.test" values={{ name: 'Martin', itemCount: this.state.itemCount }} />
                </h4>
            </Paper>
        );
    }

}

export default Homepage;
