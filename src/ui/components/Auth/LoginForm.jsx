import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';
import { Form, Field, reduxForm } from 'redux-form';

import Joi from 'joi-browser';

import TextField from '../FormFields/TextField';
import RaisedButton from '../FormFields/RaisedButton';

const texts = {
    button: {
        login: (<FormattedMessage id={'login.singIn'} />),
        facebook: (<FormattedMessage id={'login.facebook'} />),
        twitter: (<FormattedMessage id={'login.twitter'} />),
        linkedIn: (<FormattedMessage id={'login.linkedIn'} />),
    },
    email: {
        hint: (<FormattedMessage id={'login.email'} />),
    },
    password: {
        hint: (<FormattedMessage id={'login.password'} />),
    },
};

const validate = (values = {}) => {
    const errors = {};

    // incorrect
    if (Joi.validate(values.email, Joi.string().email()).error) {
        errors.email = <FormattedMessage id={'login.email.error.incorrect'} />;
    }

    // required
    if (!values.email) {
        errors.email = <FormattedMessage id={'login.email.error.isRequired'} />;
    }
    if (!values.password) {
        errors.password = <FormattedMessage id={'login.password.error.isRequired'} />;
    }

    return errors;
};

class LoginForm extends Component {

    static propTypes = {
        handleSubmit: PropTypes.func.isRequired,
        invalid: PropTypes.bool.isRequired,
        onSubmit: PropTypes.func.isRequired,
    };

    render() {
        const { handleSubmit, invalid, onSubmit } = this.props;

        return (
            <Form className="form" onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <Field name={'email'} component={TextField} hint={texts.email.hint} />
                </div>
                <div>
                    <Field name={'password'} component={TextField} type={'password'} hint={texts.password.hint} />
                </div>
                <div className="text-center">
                    <div className="form__button login__wrap">
                        <Field name="submitButton" component={RaisedButton} type={'submit'} label={texts.button.login} disabled={invalid} primary />
                    </div>
                </div>
            </Form>
        );
    }
}

export default reduxForm({
    form: 'LoginForm',
    validate,
    // TESTING INITIAL VALUES
    initialValues: {
        email: 'test@test.cz',
        password: 'password',
    },
})(LoginForm);
