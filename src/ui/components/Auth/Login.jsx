import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';

import Paper from 'material-ui/Paper';

import config from '../../config/config';
import LoginForm from './LoginForm';

class Login extends Component {

    static propTypes = {
        login: PropTypes.func.isRequired,
    };

    render() {
        return (
            <Paper className="paper">
                <h3 className="text-center"><FormattedMessage id="login.title" values={{ appName: config.appName }} /></h3>
                <LoginForm
                    onSubmit={this.props.login}
                />
            </Paper>
        );
    }

}

export default Login;
