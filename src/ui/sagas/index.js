import dialogSaga from '../sagas/DialogSagas';
import redirectSaga from '../sagas/RedirectSagas';
import requestSaga from '../sagas/RequestSagas';

/**
 * Root generator for all application sagas
 */
export default function* () {
    yield [
        requestSaga(),
        redirectSaga(),
        dialogSaga(),
    ];
}
