import { takeEvery, put } from 'redux-saga/effects';
import { replace } from 'react-router-redux';

import { SIGN_OUT } from '../constants/AuthTypes';

export function* redirectCallSaga(_action) {
    switch (_action.type) {
    case SIGN_OUT:
        // only if redirect in action is true
        if (_action.redirect) yield put(replace('/'));
        break;
    default:
    }

    return null;
}

export default function* redirectSaga() {
    yield takeEvery('*', redirectCallSaga);
}
