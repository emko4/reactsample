import { delay } from 'redux-saga';
import { takeEvery, call, put } from 'redux-saga/effects';

function isRequestAction(_action) {
    return _action.type.indexOf('REQUEST_') === 0;
}

/**
 * Generator for redux-saga for api calls
 * @param {Object} _action - redux action for api call
 */
export function* requestCallSaga(_action) {
    // simulation of async delay
    yield delay(800);

    if (_action.onSuccess) {
        yield put(typeof _action.onSuccess === 'function' ? _action.onSuccess(_action.response, _action) : _action.onSuccess);
    }

    // call onComplete function with success
    if (_action.onComplete && typeof _action.onComplete === 'function') {
        yield call(_action.onComplete, true, _action.response);
    }

    return null;
}

/**
 * Root generator for redux-saga for api calls to run them concurrently
 */
export default function* requestSaga() {
    yield takeEvery(isRequestAction, requestCallSaga);
}
