import { takeEvery, call, race, take } from 'redux-saga/effects';
import _ from 'lodash';

function isShowDialogAction(_action) {
    return _action.type.search(/^SHOW_.*_DIALOG$/gm) !== -1;
}

/**
 * Generator for redux-saga for dialog calls
 * @param {Object} _action - redux action for dialog call
 */
export function* dialogCallSaga(_action) {
    // resolve dialog type
    const dialogType = _action.type.replace(/SHOW_|_DIALOG/gm, '');

    // prepare empty race object
    let raceObject = {
        submit: null,
        hide: null,
    };

    // process all other sametype actions, while hide action came
    while (_.isEmpty(raceObject.hide)) {
        raceObject = yield race({
            submit: take(`SUBMIT_${dialogType}_DIALOG`),
            hide: take(`HIDE_${dialogType}_DIALOG`),
        });

        if (!_.isEmpty(raceObject.submit)) {
            const submitAction = raceObject.submit;

            if (submitAction.callback && _action[submitAction.callback] && typeof _action[submitAction.callback] === 'function') {
                yield call(_action[submitAction.callback], submitAction.values || {});
            }
        }
    }

    // get hide action
    const hideAction = raceObject.hide;

    // if there is callback, call it
    if (hideAction.callback && _action[hideAction.callback] && typeof _action[hideAction.callback] === 'function') {
        yield call(_action[hideAction.callback]);
    }

    return null;
}

export default function* dialogSaga() {
    yield takeEvery(isShowDialogAction, dialogCallSaga);
}
