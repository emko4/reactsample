export const USER_ROLES = {
    ADMIN: 1,
    USER: 3,
};

export const NOBODY_ACCESS = -1;
export const EVERYBODY_ACCESS = -2;
