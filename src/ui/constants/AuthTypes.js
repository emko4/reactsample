export const REQUEST_SIGN_IN = 'REQUEST_SIGN_IN';
export const SIGN_IN = 'SIGN_IN';
export const SIGN_IN_FAILED = 'SIGN_IN_FAILED';

export const SIGN_OUT = 'SIGN_OUT';
