export const REQUEST_GET_ME = 'REQUEST_GET_ME';
export const GET_ME = 'GET_ME';
export const GET_ME_FAILED = 'GET_ME_FAILED';

export const CLEAR_USER = 'CLEAR_USER';
