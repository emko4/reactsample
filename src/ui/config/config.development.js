import log from 'loglevel';

const config = {
    // dev config goes here
    devTools: true,
    logLevel: log.levels.TRACE,
};

export default config;
