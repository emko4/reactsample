import _ from 'lodash';
import log from 'loglevel';

const env = process.env.NODE_ENV;
// eslint-disable-next-line import/no-dynamic-require
const envConfig = require(`./config.${env}.js`).default;

const defaults = {
    // default configuration goes here
    appName: 'React-Redux Sample',
    devTools: true,
    logLevel: log.levels.TRACE,
};

const config = _.merge(defaults, envConfig);

export default config;
