import log from 'loglevel';

const config = {
    // stage config goes here
    devTools: false,
    logLevel: log.levels.INFO,
};

export default config;
