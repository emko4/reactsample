import log from 'loglevel';

const config = {
    // production config goes here
    devTools: false,
    logLevel: log.levels.ERROR,
};

export default config;
