import * as types from '../constants/UserTypes';
import * as ToastActions from './ToastActions';

/**
 * GET ME actions
 */

// parameter user is response body
export function getMe(user) {
    return {
        type: types.GET_ME,
        user,
    };
}

export function getMeFailed(error) {
    return [{
        type: types.GET_ME_FAILED,
    }, ToastActions.addErrorToast({
        title: 'toast.getMe.title',
        error,
    })];
}

export function getMeRequest(testResponse, onComplete = null) {
    return {
        type: types.REQUEST_GET_ME,
        onSuccess: getMe,
        onError: getMeFailed,
        onComplete,
        // test response
        response: testResponse,
    };
}

/* CLEAR USER FROM STORE action */

export function clearUser() {
    return {
        type: types.CLEAR_USER,
    };
}
