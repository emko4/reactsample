import * as types from '../constants/AuthTypes';
import * as ApplicationActions from './ApplicationActions';
import * as ToastActions from './ToastActions';
import * as UserActions from './UserActions';

/**
 * SING IN actions
 */

export function signIn(response) {
    return [{
        type: types.SIGN_IN,
        user: response.user,
    }, UserActions.getMeRequest(response.user), ToastActions.addSuccessToast({
        message: 'toast.signIn.logged',
        params: {
            username: response.user.email || '',
        },
    })];
}

export function signInFailed(error) {
    return [{
        type: types.SIGN_IN_FAILED,
    }, ToastActions.addErrorToast({
        title: 'toast.signIn.title',
        error,
    })];
}

export function signInRequest(body, onComplete = null) {
    return {
        type: types.REQUEST_SIGN_IN,
        onSuccess: signIn,
        onError: signInFailed,
        onComplete,
        // test response
        response: {
            user: {
                id: 1,
                username: body.email,
                email: body.email,
                roles: [
                    {
                        id: 1,
                        name: 'ADMIN',
                    },
                ],
            },
        },
    };
}

/**
 * SING OUT actions
 */

export function signOut(redirect = true) {
    return [{
        type: types.SIGN_OUT,
        redirect,
    },
        UserActions.clearUser(),
        ApplicationActions.menuClose()];
}
