import * as types from '../constants/ToastTypes';

/**
 * ADD TOAST actions
 */

export function addToast(toast) {
    return {
        type: types.ADD_TOAST,
        ...toast,
    };
}

export function addSuccessToast(toast) {
    return {
        type: types.ADD_TOAST,
        ...toast,
        kind: 'success',
    };
}

export function addInfoToast(toast) {
    return {
        type: types.ADD_TOAST,
        ...toast,
        kind: 'info',
    };
}

export function addWarningToast(toast) {
    return {
        type: types.ADD_TOAST,
        ...toast,
        kind: 'warning',
    };
}

export function addErrorToast(toast) {
    return {
        type: types.ADD_TOAST,
        ...toast,
        kind: 'error',
    };
}

export function addApiErrorToast(error) {
    return {
        type: types.ADD_TOAST,
        kind: 'error',
        message: 'toast.apiError.message',
        title: 'toast.apiError.title',
        params: {
            code: error.code || 500,
            caption: error.caption || '',
        },
        error,
    };
}

export function addApplicationErrorToast(error) {
    return {
        type: types.ADD_TOAST,
        kind: 'error',
        message: 'toast.applicationError.message',
        title: 'toast.applicationError.title',
        error,
    };
}

/**
 * CLEAR TOAST actions
 */

export function clearActualToast() {
    return {
        type: types.CLEAR_ACTUAL_TOAST,
    };
}
