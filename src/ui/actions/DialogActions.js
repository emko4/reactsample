import * as types from '../constants/DialogTypes';

/**
 * YES/NO DIALOG actions
 */

export function showYesNoDialog(data, onYes = null, onNo = null) {
    return {
        type: types.SHOW_YES_NO_DIALOG,
        yesNo: {
            open: true,
            ...data,
        },
        onYes,
        onNo,
    };
}

export function hideYesNoDialog(callback = null) {
    return {
        type: types.HIDE_YES_NO_DIALOG,
        callback,
    };
}
