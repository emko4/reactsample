import * as types from '../constants/TranslateTypes';

/**
 * LOCALE actions
 */

export function setLocale(locale) {
    return {
        type: types.SET_LOCALE,
        locale,
    };
}

export function loadLocale() {
    return {
        type: types.LOAD_LOCALE,
    };
}
