import * as types from '../constants/ApplicationTypes';

/**
 * MENU actions
 */

export function menuOpen() {
    return {
        type: types.MENU_OPEN,
    };
}

export function menuClose() {
    return {
        type: types.MENU_CLOSE,
    };
}

export function menuToggle() {
    return {
        type: types.MENU_TOGGLE,
    };
}
