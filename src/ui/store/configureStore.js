import { createStore, applyMiddleware, compose } from 'redux';
import { persistState } from 'redux-devtools';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';

import createSagaMiddleware from 'redux-saga';
import multi from 'redux-multi';
import imuttable from 'redux-immutable-state-invariant';
import log from 'loglevel';

import errorMiddleware from '../middleware/ErrorMiddleware';
import dialogMiddleware from '../middleware/DialogMiddleware';

import rootReducer from '../reducers';
import rootSaga from '../sagas';

import config from '../config/config';

export default function configureStore(initialState) {
    // Create middlewares
    const sagaMiddleware = createSagaMiddleware();
    const routingMiddleware = routerMiddleware(browserHistory);

    // Initialization of middlewares
    let middleware = applyMiddleware(multi, dialogMiddleware, sagaMiddleware, routingMiddleware, errorMiddleware);
    let enhancer = null;

    // DevTools by config
    if (config.devTools) {
        const middlewares = [multi, dialogMiddleware, sagaMiddleware, routingMiddleware, errorMiddleware, imuttable()];
        middleware = applyMiddleware(...middlewares);

        const getDebugSessionKey = () => {
            // By default we try to read the key from ?debug_session=<key> in the address bar
            const matches = window.location.href.match(/[?&]debug_session=([^&]+)\b/);
            return (matches && matches.length) ? matches[1] : null;
        };

        // if browser hasn't devTools => warning
        if (window.devToolsExtension) {
            enhancer = compose(
                // Middleware we want to use in development
                middleware,
                window.devToolsExtension(),
                persistState(getDebugSessionKey()),
            );
        } else {
            log.warn("You haven't devTools extension in browser!");
            enhancer = compose(
                // Middleware we want to use in development
                middleware,
                persistState(getDebugSessionKey()),
            );
        }
    // Production middlewares
    } else {
        enhancer = compose(middleware);
    }

    // Create store
    const store = createStore(rootReducer, initialState, enhancer);
    // Run saga middleware with given root saga
    sagaMiddleware.run(rootSaga);

    // Enable Webpack hot module replacement for reducers
    if (module.hot) {
        module.hot.accept('../reducers', () =>
            store.replaceReducer(rootReducer),
        );
    }

    return store;
}
