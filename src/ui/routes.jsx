import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';

import App from './containers/AppContainer';
import Login from './containers/Auth/Login';
import MainPanel from './containers/MainPanel/MainPanel';
import Homepage from './components/Homepage/Homepage';
import User from './containers/Users/User';
import UserDetail from './containers/Users/UserDetail';
import UserEdit from './containers/Users/UserEdit';

import AuthenticationFirewall from './containers/Auth/AuthentizationFirewall';
import AccessSecurity from './containers/Auth/AccessSecurity';
import NotFound from './containers/Announcement/NotFound';

export default (
    <div>
        <Route component={App}>
            <Route path="/" component={AuthenticationFirewall(AccessSecurity(MainPanel), Login)}>
                <IndexRoute component={Homepage} />
                <Route path="user" component={User}>
                    <IndexRoute component={UserDetail} />
                    <Route path="edit" component={UserEdit} />
                </Route>
            </Route>
            <Route path="404" component={NotFound} />
            <Redirect from="*" to="/404" />
        </Route>
    </div>
);
