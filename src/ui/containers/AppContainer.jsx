import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import cs from 'react-intl/locale-data/cs';

import Favicon from 'react-favicon';
import DocumentTitle from 'react-document-title';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
// import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';

import messages from '../localization';
import config from '../config/config';

import ico from '../assets/images/favicon.ico';

import * as TranslateActions from '../actions/TranslateActions';

import Toaster from './Toaster/Toaster';
import Dialog from './Dialog/Dialog';

// add locale data of used languages
addLocaleData([...en, ...cs]);

/**
 * Root application component.
 * Holds other application components informs use about whats happening through application ui.* state
 */
class App extends Component {

    static propTypes = {
        children: PropTypes.object.isRequired,
        locale: PropTypes.string.isRequired,
        translateActions: PropTypes.object.isRequired,
    };

    componentWillMount = () => {
        // load data from local storage
        this.props.translateActions.loadLocale();
    };

    render = () => {
        const theme = getMuiTheme(lightBaseTheme);

        return (
            <IntlProvider locale={this.props.locale} key={this.props.locale} messages={messages[this.props.locale]}>
                <DocumentTitle title={config.appName}>
                    <div>
                        <Favicon url={ico} />
                        <MuiThemeProvider muiTheme={theme}>
                            <div>
                                {this.props.children}
                                <Toaster />
                                <Dialog />
                            </div>
                        </MuiThemeProvider>
                    </div>
                </DocumentTitle>
            </IntlProvider>
        );
    };
}

function mapStateToProps(state) {
    return {
        locale: state.translate.locale,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        translateActions: bindActionCreators(TranslateActions, dispatch),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(App);
