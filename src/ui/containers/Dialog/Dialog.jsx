import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import MuiDialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import * as DialogActions from '../../actions/DialogActions';

class Dialog extends Component {

    static propTypes = {
        dialogActions: PropTypes.object.isRequired,
        yesNoDialog: PropTypes.object.isRequired,
    };

    render = () => {
        const yesNoDialogAction = [
            <FlatButton
                key={1}
                label={this.props.yesNoDialog.noLabel}
                primary
                onTouchTap={this.props.dialogActions.hideYesNoDialog.bind(this, 'onNo')}
                keyboardFocused />,
            <FlatButton
                key={2}
                label={this.props.yesNoDialog.yesLabel}
                primary
                onTouchTap={this.props.dialogActions.hideYesNoDialog.bind(this, 'onYes')} />,
        ];

        return (
            <div>
                <MuiDialog
                    key={'yesNoDialog'}
                    title={this.props.yesNoDialog.title}
                    open={this.props.yesNoDialog.open}
                    modal={false}
                    actions={yesNoDialogAction}
                    onRequestClose={this.props.dialogActions.hideYesNoDialog.bind(this, 'onNo')}
                >
                    {this.props.yesNoDialog.message}
                </MuiDialog>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        yesNoDialog: state.dialog.yesNo,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dialogActions: bindActionCreators(DialogActions, dispatch),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Dialog);
