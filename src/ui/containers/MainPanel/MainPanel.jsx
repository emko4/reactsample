import React, { Component, PropTypes } from 'react';
import { routerActions } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';

import _ from 'lodash';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';

import * as ApplicationActions from '../../actions/ApplicationActions';
import * as AuthActions from '../../actions/AuthActions';
import * as DialogActions from '../../actions/DialogActions';
import * as ToastActions from '../../actions/ToastActions';
import * as UserActions from '../../actions/UserActions';
import * as TranslateActions from '../../actions/TranslateActions';

import config from '../../config/config';

import LoggedUser from '../../components/Users/LoggedUser';
import Menu from '../../components/Menu/Menu';

class MainPanel extends Component {

    static propTypes = {
        appActions: PropTypes.object.isRequired,
        authActions: PropTypes.object.isRequired,
        children: PropTypes.object.isRequired,
        isFetching: PropTypes.bool.isRequired,
        user: PropTypes.object.isRequired,
    };

    toggleMenu = () => {
        this.props.appActions.menuToggle();
    };

    render = () => {
        // copy childrens with prosp and add association id to props
        const childrenWithProps = React.Children.map(this.props.children, (child) => React.cloneElement(child, {
            ..._.omit(this.props, ['children']),
        }));

        // set local texts, styles and icons
        const localTexts = {
            username: (this.props.isFetching || _.isEmpty(this.props.user)) ? '' : this.props.user.email,
        };

        const localIcons = {
            menu: <IconButton><NavigationMenu /></IconButton>,
        };

        // create logged user component
        const loggedUser = <LoggedUser username={localTexts.username} onSignOut={this.props.authActions.signOut} />;

        // render whole main panel
        return (
            <div className="main-panel">
                <AppBar
                    title={config.appName}
                    iconElementRight={loggedUser}
                    iconElementLeft={localIcons.menu}
                    onLeftIconButtonTouchTap={this.toggleMenu}
                />
                <Menu {...this.props} />
                <div className="main-panel__content">{childrenWithProps}</div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user.user,
        isFetching: state.user.isFetching,
        locale: state.translate.locale,
        menuOpen: state.application.menuOpen,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        appActions: bindActionCreators(ApplicationActions, dispatch),
        authActions: bindActionCreators(AuthActions, dispatch),
        dialogActions: bindActionCreators(DialogActions, dispatch),
        userActions: bindActionCreators(UserActions, dispatch),
        toastActions: bindActionCreators(ToastActions, dispatch),
        translateActions: bindActionCreators(TranslateActions, dispatch),
        routerActions: bindActionCreators(routerActions, dispatch),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(injectIntl(MainPanel));
