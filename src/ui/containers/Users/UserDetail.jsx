import React, { Component, PropTypes } from 'react';

import UserDetailComponent from '../../components/Users/UserDetail';

import Loader from '../../components/Loader/Loader';

class UserDetail extends Component {

    static propTypes = {
        isFetching: PropTypes.bool.isRequired,
    };

    // issue with propTypes in React.cloneElement -> https://github.com/facebook/react/issues/6653
    static defaultProps = {
        isFetching: false,
    };

    render() {
        return (
            <Loader show={this.props.isFetching}>
                <UserDetailComponent
                    key={'UserDetailComponent'}
                    {...this.props}
                />
            </Loader>
        );
    }

}

export default UserDetail;
