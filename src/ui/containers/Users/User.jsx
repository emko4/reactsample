import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reset } from 'redux-form';

import _ from 'lodash';

import * as AuthActions from '../../actions/AuthActions';
import * as ToastActions from '../../actions/ToastActions';
import * as UserActions from '../../actions/UserActions';

class User extends Component {

    static propTypes = {
        children: PropTypes.object.isRequired,
    };

    render = () => {
        const childrenWithProps = React.Children.map(this.props.children, (child) => React.cloneElement(child, {
            ..._.omit(this.props, ['children']),
        }));

        return (
            <div className="user">
                {childrenWithProps}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        isFetching: state.user.isFetching,
        user: state.user.user,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(AuthActions, dispatch),
        toastActions: bindActionCreators(ToastActions, dispatch),
        userActions: bindActionCreators(UserActions, dispatch),
        formActions: bindActionCreators({ reset }, dispatch),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(User);
