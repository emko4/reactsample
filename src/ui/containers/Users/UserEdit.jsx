import React, { Component, PropTypes } from 'react';
import { FormattedMessage } from 'react-intl';

import Paper from 'material-ui/Paper';
import { Tabs, Tab } from 'material-ui/Tabs';

import UserEditEmail from '../../components/Users/UserEditEmail';
import UserEditPassword from '../../components/Users/UserEditPassword';

import Loader from '../../components/Loader/Loader';

class UserEdit extends Component {

    static propTypes = {
        isFetching: PropTypes.bool.isRequired,
        toastActions: PropTypes.object.isRequired,
    };

    // issue with propTypes in React.cloneElement -> https://github.com/facebook/react/issues/6653
    static defaultProps = {
        isFetching: false,
        toastActions: {},
    };

    constructor(props) {
        super(props);

        this.state = {
            mql: null,
            shortName: false,
        };
    }

    componentWillMount() {
        const mql = window.matchMedia('(min-width: 800px)');
        mql.onchange = this.mediaQueryChanged;

        this.setState({
            mql,
            shortName: !mql.matches,
        });
    }

    componentWillUnmount() {
        this.state.mql.onchange = null;
    }

    mediaQueryChanged = () => {
        this.setState({
            shortName: !this.state.mql.matches,
        });
    };

    saveEmail = () => {
        this.props.toastActions.addInfoToast({
            message: 'user.changeEmail.warning',
            options: {
                progressBar: true,
            },
        });
    };

    savePassword = () => {
        this.props.toastActions.addInfoToast({
            message: 'user.changePassword.warning',
            options: {
                progressBar: true,
            },
        });
    };

    render() {
        const localTexts = {
            editEmail: (<FormattedMessage id={`user.changeEmail.title${this.state.shortName ? 'Short' : ''}`} />),
            editPassword: (<FormattedMessage id={`user.changePassword.title${this.state.shortName ? 'Short' : ''}`} />),
        };

        const isFetching = this.props.isFetching;

        return (
            <div className="edit-user-wrapper">
                <Paper>
                    <Tabs>
                        <Tab label={localTexts.editEmail} >
                            <Loader show={isFetching}>
                                <UserEditEmail key={'userEmail'} saveEmail={this.saveEmail} {...this.props} />
                            </Loader>
                        </Tab>
                        <Tab label={localTexts.editPassword} >
                            <Loader show={isFetching}>
                                <UserEditPassword key={'userPass'} savePassword={this.savePassword} {...this.props} />
                            </Loader>
                        </Tab>
                    </Tabs>
                </Paper>
            </div>
        );
    }

}

export default UserEdit;
