import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as AuthActions from '../../actions/AuthActions';
import * as TranslateActions from '../../actions/TranslateActions';

import Loader from '../../components/Loader/Loader';
import LanguageChooser from '../../components/Menu/LanguageChooser';

import LoginComponent from '../../components/Auth/Login';

class Login extends Component {

    static propTypes = {
        authActions: PropTypes.object.isRequired,
        isFetching: PropTypes.bool.isRequired,
        translateActions: PropTypes.object.isRequired,
    };

    login = (values) => {
        this.props.authActions.signInRequest({
            ...values,
        });
    };

    render() {
        return (
            <div className="login">
                <LanguageChooser outOfLayout translateActions={this.props.translateActions} />
                <Loader show={this.props.isFetching}>
                    <LoginComponent
                        key={'LoginComponent'}
                        login={this.login}
                        {...this.props}
                    />
                </Loader>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        isFetching: state.auth.isFetching,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(AuthActions, dispatch),
        translateActions: bindActionCreators(TranslateActions, dispatch),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Login);
