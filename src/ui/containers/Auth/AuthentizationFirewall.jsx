import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

export default (AuthenticatedComponent, UnauthenticatedComponent) => {
    class AuthentizationFirewall extends Component {

        static propTypes = {
            isAuthenticated: PropTypes.bool.isRequired,
        };

        render = () => {
            const isAuthenticated = this.props.isAuthenticated;
            const authenticatedContent = <AuthenticatedComponent {...this.props} />;

            if (isAuthenticated) {
                return authenticatedContent;
            }

            let unauthenticatedComponent = (
                <div>
                    <span>Authentization Firewall: You shall not pass.</span>
                    <Link to="/login">Login</Link>
                </div>
            );

            if (UnauthenticatedComponent) {
                unauthenticatedComponent = <UnauthenticatedComponent />;
            }

            return unauthenticatedComponent;
        };

    }

    function mapStateToProps(state) {
        return {
            isAuthenticated: state.auth.isAuthenticated,
        };
    }

    return connect(
        mapStateToProps
    )(AuthentizationFirewall);
};
