import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import _ from 'lodash';

import { USER_ROLES, NOBODY_ACCESS, EVERYBODY_ACCESS } from '../../constants/UserRoles';

// method for check authorization
const hasAccess = (userRoles, demandRoles = []) => {
    // without user roles, automatically assume unauthorized
    // or demand roles is NOBODY_ACCESS
    if (!userRoles || demandRoles === NOBODY_ACCESS) {
        return false;
    }

    // without demand roles, automatically assume authorized
    // or demand roles is EVERYBODY_ACCESS
    if (_.isEmpty(demandRoles) || demandRoles === EVERYBODY_ACCESS) {
        return true;
    }

    // suppose we aren't authorize
    let isAuth = false;
    // iterate over user roles
    _.each(userRoles, (role) => {
        // if user role is in demands roles
        if (_.find(demandRoles, (demandRole) => (demandRole === role.id))) {
            isAuth = true;
            // stop iteration (we have permission)
            return false;
        }
        // continue (eslint)
        return true;
    });

    // return if user is authorized
    return isAuth;
};

// default Access Security for react router (similar to AuthentizationFirewall)
export default function AccessSecurity(AccessComponent) {
    class AccessSecurityComponent extends Component {

        static propTypes = {
            user: PropTypes.object.isRequired,
        };

        render = () => {
            const access = {};

            const user = !_.isEmpty(this.props.user) ? this.props.user : false;

            // state off access
            access.loadedUser = !!user;
            access.loaded = access.loadedUser;

            // user access
            access.isAdmin = !!user && hasAccess(user.roles, [USER_ROLES.ADMIN]);

            return <AccessComponent {...this.props} access={access} />;
        };

    }

    function mapStateToProps(state) {
        return {
            user: state.user.user,
        };
    }

    return connect(mapStateToProps)(AccessSecurityComponent);
}
