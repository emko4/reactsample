import React, { Component, PropTypes } from 'react';
import { routerActions } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import _ from 'lodash';

import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';

const texts = {
    button: <FormattedMessage id={'error.notAuthorized.button'} />,
};

class NotAuthorized extends Component {

    static propTypes = {
        associationId: PropTypes.number.isRequired,
        routerActions: PropTypes.object.isRequired,
    };

    back = () => {
        if (_.isEmpty(this.props.associationId)) {
            this.props.routerActions.replace('/');
        } else {
            this.props.routerActions.replace(`/association/${this.props.associationId}`);
        }
    };

    render() {
        return (
            <div className="not-authorized">
                <Paper className="paper">
                    <h1 className="not-authorized__text"><FormattedMessage id={'error.notAuthorized.message'} /></h1>
                    <RaisedButton label={texts.button} primary onTouchTap={this.back} />
                </Paper>
            </div>
        );
    }

}

function mapDispatchToProps(dispatch) {
    return {
        routerActions: bindActionCreators(routerActions, dispatch),
    };
}

export default connect(
    null,
    mapDispatchToProps,
)(NotAuthorized);
