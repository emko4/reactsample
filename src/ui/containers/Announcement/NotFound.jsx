import React, { Component, PropTypes } from 'react';
import { routerActions } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';

const texts = {
    button: <FormattedMessage id={'error.notFound.button'} />,
};

class NotFound extends Component {

    static propTypes = {
        routerActions: PropTypes.object.isRequired,
    };

    back = (_e) => {
        _e.preventDefault();
        this.props.routerActions.replace('/');
    };

    render() {
        return (
            <div className="not-found">
                <Paper className="paper">
                    <h1 className="not-found__text"><FormattedMessage id={'error.notFound.title'} /></h1>
                    <h3 className="not-found__text"><FormattedMessage id={'error.notFound.message'} /></h3>
                    <RaisedButton label={texts.button} primary onTouchTap={this.back} />
                </Paper> .
            </div>
        );
    }

}

function mapDispatchToProps(dispatch) {
    return {
        routerActions: bindActionCreators(routerActions, dispatch),
    };
}

export default connect(
    null,
    mapDispatchToProps,
)(NotFound);
