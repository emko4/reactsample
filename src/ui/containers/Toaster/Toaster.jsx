import { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';

import _ from 'lodash';
import toastr from 'toastr';

import * as ToastActions from '../../actions/ToastActions';

class Toaster extends Component {

    static propTypes = {
        intl: PropTypes.object.isRequired,
        toastActions: PropTypes.object.isRequired,
    };

    componentWillMount() {
        // global settings of toastr
        toastr.options.positionClass = 'toast-bottom-right';
        toastr.options.timeOut = 5000;
        toastr.options.extendedTimeOut = 1000;
        toastr.options.newestOnTop = false;
    }

    componentWillReceiveProps(nextProps) {
        // if there are a messages, show them and clear in store
        if (!_.isEmpty(nextProps.actuals)) {
            _.each(nextProps.actuals, (toast) => {
                this.showToast(toast);
            });

            this.props.toastActions.clearActualToast();
        }
    }

    showToast = (toast) => {
        const message = (toast.message) ? this.props.intl.formatMessage({ id: toast.message }, toast.params) : '';
        const title = (toast.title) ? this.props.intl.formatMessage({ id: toast.title }, toast.params) : '';
        const options = (toast.options) ? toast.options : {};

        toastr[toast.kind](message, title, options);
    };

    // nothing to render
    render = () => null;
}

function mapStateToProps(state) {
    return {
        actuals: state.toast.actuals,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        toastActions: bindActionCreators(ToastActions, dispatch),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(injectIntl(Toaster));
