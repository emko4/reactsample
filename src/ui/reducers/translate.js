import * as types from '../constants/TranslateTypes';

const LocalStorageHandlers = {
    loadLocale: () => localStorage.getItem('locale'),
    saveLocale: (locale) => {
        localStorage.setItem('locale', locale);
    },
};

const initialState = {
    locale: 'en',
};

export default function (state = initialState, action) {
    switch (action.type) {
    case types.SET_LOCALE:
        LocalStorageHandlers.saveLocale(action.locale);
        return {
            ...state,
            locale: action.locale,
        };
    case types.LOAD_LOCALE:
        return {
            ...state,
            locale: LocalStorageHandlers.loadLocale() || state.locale,
        };
    default:
        return state;
    }
}

