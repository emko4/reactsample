import * as types from '../constants/DialogTypes';

/*
* Dialog object MUST have camel case format of key from constants
* For example type YES_NO -> yesNo, FORM -> form
*/
const initialState = {
    yesNo: {
        open: false,
        title: '',
        message: '',
        yesLabel: 'Yes',
        noLabel: 'No',
    },
};

export default function (state = initialState, action) {
    switch (action.type) {

    /**
     * YES/NO DIALOG
     */

    case types.SHOW_YES_NO_DIALOG:
        return {
            ...state,
            yesNo: {
                ...state.yesNo,
                ...action.yesNo,
            },
        };
    case types.HIDE_YES_NO_DIALOG:
        return {
            ...state,
            yesNo: initialState.yesNo,
        };

    default:
        return state;
    }
}
