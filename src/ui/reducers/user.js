import * as types from '../constants/UserTypes';

const initialState = {
    user: {}, // current user data obtained from me request
    isFetching: false,
};

export default function (state = initialState, action) {
    switch (action.type) {
    // Call request for user data
    case types.REQUEST_GET_ME:
        return {
            ...state,
            isFetching: true,
        };
    // Set user data to store
    case types.GET_ME:
        return {
            ...state,
            user: action.user,
            isFetching: false,
        };
    // Reset user data and fetching
    case types.GET_ME_FAILED:
        return {
            ...state,
            user: initialState.user,
            isFetching: false,
        };
    // Clear user data from store
    case types.CLEAR_USER:
        return {
            ...initialState,
        };
    default:
        return state;
    }
}
