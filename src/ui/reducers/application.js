import * as types from '../constants/ApplicationTypes';

const initialState = {
    menuOpen: false,
};

export default function (state = initialState, action) {
    switch (action.type) {
    case types.MENU_OPEN:
        return {
            ...state,
            menuOpen: true,
        };
    case types.MENU_CLOSE:
        return {
            ...state,
            menuOpen: false,
        };
    case types.MENU_TOGGLE:
        return {
            ...state,
            menuOpen: !state.menuOpen,
        };
    default:
        return state;
    }
}
