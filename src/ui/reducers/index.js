import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import application from './application';
import auth from './auth';
import dialog from './dialog';
import user from './user';
import toast from './toast';
import translate from './translate';

const rootReducer = combineReducers({
    application,
    auth,
    dialog,
    user,
    toast,
    translate,
    routing: routerReducer,
    form: formReducer,
});

export default rootReducer;
