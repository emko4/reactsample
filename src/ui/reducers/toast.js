import _ from 'lodash';

import * as types from '../constants/ToastTypes';

const initialState = {
    toasts: [],
    actuals: [],
};

export default function (state = initialState, action) {
    let toast = null;

    switch (action.type) {
    case types.ADD_TOAST:
        toast = _.assign(_.omit(action, ['type']), { timestamp: Date.now() });
        return {
            ...state,
            toasts: [toast, ...state.toasts],
            actuals: [...state.actuals, toast],
        };
    case types.CLEAR_ACTUAL_TOAST:
        return {
            ...state,
            actuals: initialState.actuals,
        };
    default:
        return state;
    }
}
