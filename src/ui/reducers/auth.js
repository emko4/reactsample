import _ from 'lodash';

import * as types from '../constants/AuthTypes';

const initialState = {
    user: null, // current user data obtained from login
    isAuthenticated: false,
    isFetching: false, // fetching flag
};

export default function (state = initialState, action) {
    switch (action.type) {
    // Call requests for sign in
    case types.REQUEST_SIGN_IN:
        return {
            ...state,
            isFetching: true,
        };
    // Set authentification data to store
    case types.SIGN_IN:
        return {
            ...state,
            ..._.omit(action, ['type']),
            isAuthenticated: true,
            isFetching: false,
        };
    // Reset state to initial
    case types.SIGN_IN_FAILED:
        return {
            ...initialState,
        };
    // Clear authentification data without locale
    case types.SIGN_OUT:
        return {
            ...initialState,
        };
    default:
        return state;
    }
}
