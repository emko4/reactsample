// Project
import 'babel-polyfill';
import 'isomorphic-fetch';

import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import log from 'loglevel';

import 'bootstrap-sass';
import './styles/main.scss';

import config from './config/config';
import configureStore from './store/configureStore';
import routes from './routes';

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

const rootElement = document.getElementById('app');

// react-tap-event-plugin
injectTapEventPlugin();

// set log level from config by NODE_ENV
log.setLevel(config.logLevel);

// Render the React application to the DOM
ReactDOM.render(
    <Provider store={store}>
        <Router history={history} routes={routes} />
    </Provider>,
    rootElement,
);
