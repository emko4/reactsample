
import _ from 'lodash';
import log from 'loglevel';

function isShowDialogAction(_action) {
    return _action.type.search(/^SHOW_.*_DIALOG$/gm) !== -1;
}

export default function dialogMiddleware(_store) {
    return (_next) => (_action) => {
        // if action isn't show dialog action do nothing
        if (!isShowDialogAction(_action)) {
            _next(_action);
            return;
        }

        // get current state
        const state = _store.getState();

        // get dialog type
        const dialogType = _action.type.replace(/SHOW_|_DIALOG/gm, '');

        // get dialog data from state
        const dialogData = state.dialog[_.camelCase(dialogType)];

        // Check dialog data
        if (!dialogData) {
            log.error(`Bad type of dialog (${dialogType})!!! Action is canceled.`);
            return;
        }

        // if there is open dialog of same type, cancel current show action
        if (!dialogData.open) {
            _next(_action);
        } else {
            log.warn(`Same dialog type (${dialogType}) cannot be open twice in same time!!! Second dialog is canceled.`);
        }
    };
}
