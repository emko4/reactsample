import log from 'loglevel';

import * as actions from '../actions/ToastActions';

export default function errorMiddleware(_store) {
    return (_next) => (_action) => {
        try {
            _next(_action);
        } catch (_error) {
            log.error(_error);
            _store.dispatch(actions.addApplicationErrorToast(_error));
        }
    };
}
